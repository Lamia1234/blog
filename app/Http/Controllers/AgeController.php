<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;

use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class AgeController extends Controller
{
    public function index()
    {
        //echo "This is HomeController page";
        return view('contact');
    }
    public function store(Request $request){
         $validatedData = $request->validate([
        'name' => 'required|max:255|min:3',
        'email' => 'required|unique:student',
    ]);
        $data=array();
        $data['name']=$request->name;
        $data['email']=$request->email;
        //print_r($data);
        $student=DB::table('student')->insert($data);
        if($student){
            $notification = array(
    'message' => "Data insert successful",
    'alert-type' => 'success'
);

return redirect()->route('lamia/index')->with($notification);
            
        }
        else{
                $notification = array(
    'message' => "Data insert failed",
    'alert-type' => 'error'
);

return redirect()->back('')->with($notification);
            
        
    }
}
    public function lamia(){
        $data1=DB::table('student')->get();
        return view('alldata',compact('data1'));
    }
      public function show($id){
        $data1=DB::table('student')->where('id',$id)->first();
        return view('view',compact('data1'));
    }
    
     public function destroy($id){
        $data1=DB::table('student')->where('id',$id)->delete();
            if($data1){
                $notification = array(
            'message' => "Data destroy successful",
            'alert-type' => 'success'
        );

        return redirect()->route('lamia/index')->with($notification);
                    
                }
                else{
                $notification = array(
            'message' => "Data destroy failed",
            'alert-type' => 'error'
        );

return redirect()->route('lamia/index')->with($notification);
            
    }
}
public function edit($id){
    $data1=DB::table('student')->where('id',$id)->first();
        return view('edit',compact('data1'));
}
public function update(Request $request,$id){
         $validatedData = $request->validate([
        'name' => 'required|max:255|min:3',
        'email' => 'required',
    ]);
        $data=array();
        $data['name']=$request->name;
        $data['email']=$request->email;
        //print_r($data);
        $student=DB::table('student')->where('id',$id)->update($data);
        if($student){
            $notification = array(
    'message' => "Data update successful",
    'alert-type' => 'success'
);

return redirect()->route('lamia/index')->with($notification);
            
        }
            else{
                $notification = array(
    'message' => "No update occured",
    'alert-type' => 'error'
);

return redirect()->route('lamia/index')->with($notification);
            
        
    }
}
public function image(){
    $data=DB::table('student')->get();
        return view('image',compact('data'));
    
}
public function imagestore(Request $request){

 $validatedData = $request->validate([
        'reg_id' => 'required',
        'image' => 'required |mimes:jpeg,jpg,png |max:1000',
    ]);
 $data=array();
 $data['reg_id']=$request->reg_id;
 $image=$request->file('image');
 $image_name=hexdec(uniqid());
 $ext=strtolower($image->getClientOriginalExtension());
 $new_image=$image_name.'.'.$ext;
 $path='public/image/';
 $img_url=$path. $new_image;
 $success=$image->move($path,$new_image);
 $data['image']=$img_url;
 $student=DB::table('registration')->insert($data);
        if($student){
            $notification = array(
    'message' => "image upload successful",
    'alert-type' => 'success'
);

return redirect()->route('lamia/index')->with($notification);
            
        }
        else{
                $notification = array(
    'message' => "image upload failed",
    'alert-type' => 'error'
);

return redirect()->back('')->with($notification);
            
        
    }
}
public function imageall(){
    //$lamia=DB::table('registration')->get();
    $lamia=DB::table('registration')->join('student','registration.reg_id','student.id')->select('registration.*',
        'student.name')->get();
    
    return view('alluser',compact('lamia'));
}
public function imagesingle($id){
    $data1=DB::table('registration')->join('student','registration.reg_id','student.id')->select('registration.*',
        'student.name')->where('registration.id',$id)->first();
    
    return view('singleimage',compact('data1'));

}
public function imagedelete($id){
    $image=DB::table('registration')->where('id',$id)->first();
    $imgpath=$image->image;
        $data1=DB::table('registration')->where('id',$id)->delete();
         if($data1){
                $notification = array(
            'message' => "Data destroy successful",
            'alert-type' => 'success'
        );

        return redirect()->route('alluser')->with($notification);
                    
                }
                else{
                $notification = array(
            'message' => "Data destroy failed",
            'alert-type' => 'error'
        );

return redirect()->route('alluser')->with($notification);
            
    }

}
public function imageedit($id){
    $student=DB::table('student')->get();
    $registration=DB::table('registration')->where('id',$id)->first();
    return view('editimage',compact('student','registration'));

}
public function imageupdate(Request $request,$id){
         $validatedData = $request->validate([
        'reg_id' => 'required',
        'image' => 'mimes:jpeg,jpg,png |max:1000',
    ]);
        $data=array();
 $data['reg_id']=$request->reg_id;
 $image=$request->file('image');
 $image_name=hexdec(uniqid());
 $ext=strtolower($image->getClientOriginalExtension());
 $new_image=$image_name.'.'.$ext;
 $path='public/image/';
 $img_url=$path. $new_image;
 $success=$image->move($path,$new_image);
 $old_path=$request->old_image;
 unlink($old_path);
 $data['image']=$img_url;
        $student=DB::table('regisration')->where('id',$id)->update($data);
        if($student){
            $notification = array(
    'message' => "Data update successful",
    'alert-type' => 'success'
);

return redirect()->route('image.update')->with($notification);
            
        }
            else{
                $data['image']=$request->old_image;
                $student=DB::table('regisration')->where('id',$id)->update($data);
                $notification = array(
    'message' => "No update occured",
    'alert-type' => 'error'
);

return redirect()->route('image.update')->with($notification);
            
        
    }
}

public function PDF()
    {
        $data = DB::table('student')->get();
        $pdf = PDF::loadView('myPDF', compact('data'));
  
        return $pdf->download('myPDF.pdf');
    }
    public function excel()
    {
       return Excel::download(new UsersExport, 'users.xlsx');
    }

}
