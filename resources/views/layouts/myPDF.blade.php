@extends('layout')

@section('content')
	<section class="page-section" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Registration Form</h2>
         
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
        <table class="table table-stripped table-light" id="myTable">
  <thead class ="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Action</th>
    
    </tr>
  </thead>
  <tbody>
    @foreach($data1 as $mostafa)
    <tr>
      <th scope="row">{{$mostafa->id}}</th>
      <td>{{$mostafa->name}}</td>
      <td>{{$mostafa->email}}</td>
      <td>
        <a class="btn btn-info" href="{{url('single/view/'.$mostafa->id)}}" role="button">View</a>
        <a class="btn btn-warning" href="{{url('single/edit/'.$mostafa->id)}}" role="button">Edit</a>
        <a class="btn btn-danger" href="{{url('single/delete/'.$mostafa->id)}}" role="button">Delete</a>
      </td>
    
    </tr>
   
    @endforeach
  </tbody>
</table>
</div>
</div>
</div>
</section>
@endsection

         	
