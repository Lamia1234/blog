@extends('layout')

@section('content')
	<section class="page-section" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Image upload</h2>
         
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
          <form action="{{url('image/upate/'.$registration->id)}}" enctype="multipart/form-data"
          method="post">
          @csrf
            
                <div class="form-group">
                  <label for ="name">Name</label>
                  <select name="reg_id" class="form_control"id="exampleFormControlSelect1" >
                @foreach ($student as $row)
                  <option value="{{$row->id}}"@if($row->id==$registration->reg_id)
                  	{{"selected"}}>
                  	
                  	 @endif{{$row->name}}</option>
                 @endforeach
           
              
              </select>
            </div>
             <div class="form-group">
                  <label for ="exampleFormControlFile1">Profile picture</label>
                  <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                    <label for ="exampleFormControlFile1">Profile picture</label>
                    <img src="{{url($registration->image)}}"style="width: 200px; height:200 px">
                    
                    <input type="hidden" value="{{$registration->image}}" name="old_image">
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
          </form>
        </div>
      </div>
    </div>
  </section>

@endsection

         	
