@extends('layout')

@section('content')
	<section class="page-section" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">All user Form</h2>
         
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
        <table class="table table-stripped table-light" id="myTable">
  <thead class ="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">name</th>
      <th scope="col">image</th>
    
    </tr>
  </thead>
  <tbody>
    @foreach($lamia as $mostafa)
    <tr>
      <th scope="row">{{$mostafa->id}}</th>
      <td>{{$mostafa->name}}</td>
   
      <td><img src="{{url($mostafa->image)}}" style="height:200px; width: 200px"</td>
      <td>
        <a class="btn btn-info" href="{{url('image/single/'.$mostafa->id)}}" role="button">View</a>
        <a class="btn btn-warning" href="{{url('image/edit/'.$mostafa->id)}}" role="button">Edit</a>
        <a class="btn btn-danger" href="{{url('/image/delete/'.$mostafa->id)}}" role="button">Delete</a>
      </td>
    
    </tr>
   
    @endforeach
  </tbody>
</table>
</div>
</div>
</div>
</section>
@endsection

         	
