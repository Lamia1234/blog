@extends('layout')

@section('content')
	<section class="page-section" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Registration Form</h2>
         
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
          <form action=""
          method="post">
          @csrf
            
                <div class="form-group">
                  <label for ="name">Name</label>
                  <input type="text" class="form_control"id="name" name="name" placeholder="Enter Name "> 
                 
                </div>
                <div class="form-group">
                  <label for ="email">Email</label>
                  <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                  
                </div>
                
              
              
              <div class="clearfix"></div>
              <div class="col-lg-12 text-center">
                <div id="success"></div>
                <button id="submit" class="btn btn-primary btn-xl text-uppercase" type="submit">submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

@endsection

         	
