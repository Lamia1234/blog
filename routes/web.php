<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
     return view('auth.login');


});
//Route::get('/about', function () {
    //return view('about');

//});
Route::get('/age', function () {
    return view('age');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/contact', function () {
    return view('contact');
});

//Route::get('/home', function(){
//	echo "Hello bangladesh";
//	});
Route::get('/bye', function () {
    echo "Hello bye";
 });//middleware('age');


//Route::get('/home2','HomeController@index');
Route::get('/house','ageController@index');
Route::get('/lamia',function(){
return view('about ', ['project'=> '    MOSTAFA LTD']);
});
Route::get('/contactus', 'AgeController@index')->name('contact');
Route::post('/store','AgeController@store')->name('store');
//All data view
Route::get('lamia/index','AgeController@lamia')->name('lamia/index');
//single data view
Route::get('single/view/{id}','AgeController@show');
//delete
Route::get('single/delete/{id}','AgeController@destroy');
//edit
Route::get('single/edit/{id}','AgeController@edit');
Route::post('single/update/{id}','AgeController@update');
//Route::get('/image','HomeController@image');
//imagestore
Route::post('image/store','AgeController@imagestore')->name('image.store');

Route::get('pic', 'AgeController@image')->middleware('verified');
Route::get('/imageall','AgeController@imageall')->middleware('verified');
Route::get('image/single/{id}','AgeController@imagesingle')->name('image.single');
Route::get('/image/delete/{id}','AgeController@imagedelete');
Route::get('allimage', 'AgeController@imageall')->name('alluser');
Route::get('image/edit/{id}','AgeController@imageedit');
//->name('image.single');
Route::post('image/update/{id}','AgeController@imageupdate')->name('image.update');

Auth::routes(['verify' => true]);
Route::get('/home','HomeController@index')->name('home')->middleware('verified');
Route::get('/pdf','AgeController@pdf');
Route::get('/excel/export','AgeController@excel')->name('excel.export');

























Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
